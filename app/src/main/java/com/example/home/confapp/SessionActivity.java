package com.example.home.confapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class SessionActivity extends AppCompatActivity implements View.OnClickListener{

    private TextView TitleText;
    private TextView DescriptionText;
    private TextView SpeakerText;
    private TextView TimeText;
    private TextView PresentationText;
    private Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session);

        Intent intent = getIntent();
        session = (Session) intent.getSerializableExtra("Session");

        TitleText = (TextView) findViewById(R.id.TitleTextView);
        DescriptionText = (TextView) findViewById(R.id.DescriptionTextView);
        SpeakerText = (TextView) findViewById(R.id.SpeakerTextView);
        TimeText = (TextView) findViewById(R.id.TimeTextView);
        PresentationText = (TextView) findViewById(R.id.PresentationTextView);

        fillText();
    }

    private void fillText(){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd kk:mm");
        String start = df.format(session.get_startTime().getTime());
        String end = df.format(session.get_endTime().getTime());
        TitleText.setText(String.format("Title: %s", session.get_title()));
        DescriptionText.setText(String.format("Description: %s", session.get_description()));
        SpeakerText.setText(String.format("Speaker: %s", session.get_speaker()));
        TimeText.setText("Time: FROM "+start+" TO "+end);
        if (session.get_presentation()!=null){
            PresentationText.setText(String.format("Presentation: %s", session.get_presentation()));
        }
    }

    @Override
    public void onClick(View v) {
        Calendar cal_alarm = Calendar.getInstance();
        Calendar currentTime = Calendar.getInstance();
        cal_alarm.setTime(session.get_startTime().getTime());

        if (cal_alarm.before(currentTime)){
            cal_alarm.setTime(session.get_endTime().getTime());
            if (cal_alarm.before(currentTime)){
                Toast.makeText(this.getApplicationContext(), "Session is finished", Toast.LENGTH_LONG).show();
            } else Toast.makeText(this.getApplicationContext(), "Session has started", Toast.LENGTH_LONG).show();
            return;
        }

        Intent myIntent = new Intent(this, AlarmReceiver.class);
        myIntent.putExtra("SessionTitle", session.get_title());

        PendingIntent pendingIntent = PendingIntent.getBroadcast(this.getApplicationContext(), 1253, myIntent, PendingIntent.FLAG_UPDATE_CURRENT|  Intent.FILL_IN_DATA);

        AlarmManager manager = (AlarmManager) getSystemService(ALARM_SERVICE);
        String s = session.get_title();

        manager.set(AlarmManager.RTC_WAKEUP, cal_alarm.getTimeInMillis()-10*60*1000, pendingIntent);

        Toast.makeText(this.getApplicationContext(), "Notification set", Toast.LENGTH_LONG).show();
    }
}
