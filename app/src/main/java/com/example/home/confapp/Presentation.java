package com.example.home.confapp;

import java.io.Serializable;

public class Presentation implements Serializable{

    private int _id;
    private String _title;
    private String _description;
    private String _type;

    public Presentation(int id, String title, String description, String type){
        _id = id;
        _title = title;
        _description = description;
        _type = type;
    }

    public int get_id() {
        return _id;
    }

    public String get_title() {
        return _title;
    }

    public String get_description() {
        return _description;
    }

    public String get_type() {
        return _type;
    }

    public String toString(){
        return _title+"\nDescription: "+_description+"\nType: "+_type;
    }
}
