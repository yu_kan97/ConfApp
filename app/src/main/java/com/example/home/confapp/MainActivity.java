package com.example.home.confapp;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class MainActivity extends AppCompatActivity{

    private String conferenceFile;
    private List<Session> sessionsList;
    private TableLayout tableLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tableLayout = (TableLayout) findViewById(R.id.MainTable);

        sessionsList = new ArrayList<>();

        InputStream myInput = getResources().openRawResource(R.raw.conference);
        BufferedReader reader = new BufferedReader(new InputStreamReader(myInput));

        try {
            conferenceFile = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        jsonParser();
        createTableLayout();
    }

    private void jsonParser(){
        JSONObject obj = null;
        try {
            obj = new JSONObject(conferenceFile);

            JSONArray sessions = obj.getJSONArray("sessions");
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");

            for (int i = 0; i < sessions.length(); i++)
            {
                String title = sessions.getJSONObject(i).getString("title");
                String description = sessions.getJSONObject(i).getString("description");
                String speaker = sessions.getJSONObject(i).getString("speaker");
                String startTime = sessions.getJSONObject(i).getString("startTime").substring(0, 19).replace("T", " ");
                String endTime = sessions.getJSONObject(i).getString("endTime").substring(0, 19).replace("T", " ");
                JSONArray presentations = sessions.getJSONObject(i).getJSONArray("presentations");

                Date startDate = null;
                Date endDate = null;
                try {
                    startDate = df.parse(startTime);
                    endDate = df.parse(endTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                    continue;
                }
                Calendar startCal = Calendar.getInstance();
                Calendar endCal = Calendar.getInstance();
                startCal.setTime(startDate);
                endCal.setTime(endDate);

                if (presentations.length() == 0){


                    sessionsList.add(new Session(title, description, speaker, startCal, endCal));
                } else {
                    int presentation_id = presentations.getJSONObject(0).getInt("id");
                    String presentation_title = presentations.getJSONObject(0).getString("title");
                    String presentation_description = presentations.getJSONObject(0).getString("description");
                    String presentation_type = presentations.getJSONObject(0).getString("type");
                    Presentation presentation = new Presentation(presentation_id, presentation_title, presentation_description, presentation_type);
                    sessionsList.add(new Session(title, description, speaker, startCal, endCal, presentation));
                }
            }
            Collections.sort(sessionsList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void createTableLayout(){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        Set<String> daysOfConference = new TreeSet<>();
        for (Session s : sessionsList){
            Date date = s.get_startTime().getTime();
            daysOfConference.add(sdf.format(date));
            date = s.get_endTime().getTime();
            daysOfConference.add(sdf.format(date));
        }

        TableRow headerRow = new TableRow(this);
        headerRow.setBackgroundColor(Color.BLACK);

        TextView headerView1 = new TextView(this);
        TableRow.LayoutParams loparams = new TableRow.LayoutParams();
        loparams.weight = 1;
        loparams.width = 0;
        loparams.height = TableRow.LayoutParams.MATCH_PARENT;
        loparams.setMargins(0, 0, 2, 2);//2px right-margin
        LinearLayout cell = new LinearLayout(this);
        cell.setBackgroundColor(Color.WHITE);
        cell.setLayoutParams(loparams);
        headerView1.setText(" ");
        headerView1.setPadding(0, 0, 4, 3);
        cell.addView(headerView1);
        headerRow.addView(cell);

        int sumWeight = 1;

        TableRow.LayoutParams headerLoparams = new TableRow.LayoutParams();
        headerLoparams.weight = 2;
        headerLoparams.width = 0;
        headerLoparams.height = TableRow.LayoutParams.MATCH_PARENT;
        headerLoparams.setMargins(0, 0, 2, 2);//2px right-margin
        for (String s : daysOfConference){
            LinearLayout headerCell = new LinearLayout(this);
            headerCell.setBackgroundColor(Color.WHITE);
            headerCell.setLayoutParams(headerLoparams);
            TextView headerViewDay = new TextView(this);
            headerViewDay.setText(s);
            headerViewDay.setPadding(0, 0, 4, 3);
            headerCell.addView(headerViewDay);
            headerRow.addView(headerCell);
            sumWeight += 2;
        }

        headerRow.setWeightSum(sumWeight);
        tableLayout.addView(headerRow);

        SimpleDateFormat timeFormat = new SimpleDateFormat("kk:mm");

        Set<String> timeOfSessions = new TreeSet<>();
        for (Session s : sessionsList){
            Date date = s.get_startTime().getTime();
            timeOfSessions.add(timeFormat.format(date));
            date = s.get_endTime().getTime();
            timeOfSessions.add(timeFormat.format(date));
        }

        List<TableRow> timeRows = new ArrayList<>();

        Object[] arr = timeOfSessions.toArray();
        String[] timesArray = new String[arr.length];
        for (int i=0; i<arr.length; i++){
            timesArray[i] = (String) arr[i];
        }

        for(int i=0; i<timesArray.length-1; i++){
            TableRow timeRow = new TableRow(this);
            timeRows.add(timeRow);
            timeRow.setBackgroundColor(Color.BLACK);
            timeRow.setWeightSum(sumWeight);

            LinearLayout timeCell = new LinearLayout(this);
            timeCell.setBackgroundColor(Color.WHITE);
            timeCell.setLayoutParams(loparams);
            TextView timeText = new TextView(this);
            timeText.setText(timesArray[i]+"-"+timesArray[i+1]);
            timeText.setPadding(0, 0, 4, 3);
            timeCell.addView(timeText);
            timeRow.addView(timeCell);
            tableLayout.addView(timeRow);
        }

        int[][] numberOfCells = new int[daysOfConference.size()][timeRows.size()];
        final List<Session>[][] cellsText = new ArrayList[daysOfConference.size()][timeRows.size()];
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");

        for (int i=0; i<numberOfCells.length; i++){
            for (int j=0; j<numberOfCells[i].length; j++){
                cellsText[i][j] = new ArrayList<>();
                Date d = null;
                try {
                    d = df.parse((String)(daysOfConference.toArray())[i]+ " " +timesArray[j] + ":01");
                } catch (ParseException e) {
                    e.printStackTrace();
                    continue;
                }
                Calendar timeCell = Calendar.getInstance();
                timeCell.setTime(d);

                for (Session session : sessionsList){
                    if (timeCell.after(session.get_startTime()) && timeCell.before(session.get_endTime())){
                        numberOfCells[i][j]++;
                        cellsText[i][j].add(session);
                    }
                }
            }
        }

        TableRow.LayoutParams innerLoparams = new TableRow.LayoutParams();
        innerLoparams.width=0;
        innerLoparams.height = TableRow.LayoutParams.MATCH_PARENT;
        innerLoparams.setMargins(0, 0, 2, 0);

        for (int i=0; i<numberOfCells.length; i++) {
            for (int j = 0; j < numberOfCells[i].length; j++) {
                if (numberOfCells[i][j]==0) continue;

                TableRow timeRow = timeRows.get(j);

                final LinearLayout timeCell = new LinearLayout(this);
                timeCell.setBackgroundColor(Color.WHITE);
                timeCell.setLayoutParams(headerLoparams);
                if (numberOfCells[i][j]==1){
                    TextView timeText = new TextView(this);
                    timeText.setText(cellsText[i][j].get(0).get_title());
                    timeText.setPadding(0, 0, 4, 3);
                    final int finalI = i;
                    final int finalJ = j;
                    timeCell.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent sessionIntent = new Intent(MainActivity.this, SessionActivity.class);
                            sessionIntent.putExtra("Session", cellsText[finalI][finalJ].get(0));
                            startActivity(sessionIntent);
                        }
                    });
                    timeCell.addView(timeText);
                } else {
                    for (int x = 0; x < numberOfCells[i][j]; x++) {
                        timeCell.setBackgroundColor(Color.BLACK);
                        innerLoparams.weight = headerLoparams.weight/numberOfCells[i][j];
                        LinearLayout innerCell = new LinearLayout(this);
                        innerCell.setBackgroundColor(Color.WHITE);
                        innerCell.setLayoutParams(innerLoparams);

                        TextView timeText = new TextView(this);
                        timeText.setText(cellsText[i][j].get(x).get_title());
                        //timeText.setPadding(0, 0, 0, 3);
                        innerCell.addView(timeText);
                        final int finalI = i;
                        final int finalJ = j;
                        final int finalX = x;
                        timeCell.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent sessionIntent = new Intent(MainActivity.this, SessionActivity.class);
                                sessionIntent.putExtra("Session", cellsText[finalI][finalJ].get(finalX));
                                startActivity(sessionIntent);
                            }
                        });
                        timeCell.addView(innerCell);
                    }
                }
                timeRow.addView(timeCell);
            }
        }


    }
}
