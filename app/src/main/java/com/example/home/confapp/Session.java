package com.example.home.confapp;

import android.support.annotation.NonNull;

import java.io.Serializable;
import java.util.Calendar;

public class Session implements Comparable, Serializable {

    private static int _ID=1;

    private int _id;
    private String _title;
    private String _description;
    private String _speaker;
    private Calendar _startTime;
    private Calendar _endTime;
    private Presentation _presentation;

    public Session (String title, String description, String speaker, Calendar starttime, Calendar endtime, Presentation presentation){
        _id = _ID++;
        _title = title;
        _description = description;
        _speaker = speaker;
        _startTime = starttime;
        _endTime = endtime;
        _presentation = presentation;
    }

    public Session (String title, String description, String speaker, Calendar starttime, Calendar endtime){
        this(title, description, speaker, starttime, endtime, null);
    }

    public int get_id() {
        return _id;
    }

    public String get_title() {
        return _title;
    }

    public String get_description() {
        return _description;
    }

    public String get_speaker() {
        return _speaker;
    }

    public Calendar get_startTime() {
        return _startTime;
    }

    public Calendar get_endTime() {
        return _endTime;
    }

    public Presentation get_presentation() {
        return _presentation;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        try {
            return _startTime.compareTo(((Session) o).get_startTime());
        } catch (Exception e){
            return 0;
        }
    }
}
